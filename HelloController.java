package mydomain.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {
	
	  @Autowired
   public InsuranceService insuranceService;

	
	@RequestMapping("/hello")
	public ModelAndView hello() {
		String message = "Hello!";
		return new ModelAndView("hello", "message", message);

	}
	
		@RequestMapping(value = "/applyinsurance", method = RequestMethod.GET)
		public ModelAndView applyinsurance(HttpServletRequest request, HttpServletResponse response,
		@ModelAttribute("candidate") Candidate candidate) {
		insuranceService.register(candidate);
		return new ModelAndView("hello", "amount", candidate.getInsuranceAmount());

		  }
}