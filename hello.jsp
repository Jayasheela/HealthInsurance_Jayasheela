!DOCTYPE html>
<html>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	<head>
		<title>Health Insurance</title>
	</head>
	<body>
	<div ng-app="myApp" ng-controller="myCtrl">
  		<p>Name : <input type="text" ng-model="name"></p>
 			 <h1>Hello {{name}}</h1>
 		<select ng-model="selectedGender" ng-options="x for x in names">
		</select>	 
		
		<p>Age : <input type="text" ng-model="age"></p>
		<p>Current Health : </p>
		<p>Hypertension : <select ng-model="hyperTension" ng-options="x for x in hyperTension"></p>
		<p>Blood Pressure : <select ng-model="bloodPressure" ng-options="x for x in bloodPressure"></p>
		<p>Blood Sugar : <select ng-model="bloodSugar" ng-options="x for x in bloodSugar"></p>
		<p>Over Weight : <select ng-model="overWeight" ng-options="x for x in overWeight"></p>
		
		<p>Habits : </p>
		<p>Smoking : <select ng-model="smoking" ng-options="x for x in smoking"></p>
		<p>Alcohol : <select ng-model="alcohol" ng-options="x for x in alcohol"></p>
		<p>Daily exercise: : <select ng-model="dailyExercise" ng-options="x for x in dailyExercise"></p>
		<p>Drugs : <select ng-model="drugs" ng-options="x for x in drugs"></p>
		
		<button ng-click="submitDetails()" ng-model="amount"><a href="applyinsurance"></a>Submit</button>
			
 			
 			<h1>Output:</h1>
 			
    	    <p>Health Insurance Premium for {{name}}: {{amount}}</p> 
    	    
      </div>
		<!-- ${message} -->
		
		<script>
		var app = angular.module('myApp', []);
		app.controller('myCtrl', function($scope, $http) {
    	$scope.selectedGender = ["Male", "Female"];
    	$scope.hyperTension = ["Yes", "no"];
    	$scope.bloodPressure = ["Yes", "no"];
    	$scope.bloodSugar = ["Yes", "no"];
    	$scope.overWeight = ["Yes", "no"];
    	$scope.smoking = ["Yes", "no"];
    	$scope.alcohol = ["Yes", "no"];
    	$scope.dailyExercise = ["Yes", "no"];
    	$scope.drugs = ["Yes", "no"];
    	
    	$scope.submitDetails = function() {
    		$http({	
    			  method: 'GET',
    			  url: '/applyinsurance' 
    			}).then(function successCallback(response) {
    				$scope.amount = response.data.amount;
    			  }, function errorCallback(response) {
    			    
    			  });
            return $scope.amount;
        };
    	
});
</script> 
		
		
	</body>
</html>