package mydomain.model;


public class Candidate {
	
	
	  private String name;
	  
	  private int age;

	  private String selectedGender;
	  
	  private String hyperTension;
	  
	  private String bloodPressure;
	  
	  private String bloodSugar;
	  
	  private String overWeight;
	  
	  private String smoking;
	  
	  private String alcohol;
	  
	  private String dailyExercise;
	  
	  private String drugs;
	  
	  private int insuranceAmount;
	  
	  
	  public String getName() {

		  return name;

		  }

		  public void setName(String name) {

		  this.name = name;

		  }
		  
		  
		  
		  public String getselectedGender() {

			  return selectedGender;

			  }

			  public void setSelectedGender(String selectedGender) {

			  this.selectedGender = selectedGender;

			  }
			  
			  
			  
			  public String getHyperTension() {

				  return hyperTension;

				  }

				  public void setHyperTension(String hyperTension) {

				  this.hyperTension = hyperTension;

				  }
				  
				  public String getBloodPressure() {

					  return bloodPressure;

					  }

					  public void setBloodPressure(String bloodPressure) {

					  this.bloodPressure = bloodPressure;

					  }
					  
					  public String getBloodSugar() {

						  return bloodSugar;

						  }

						  public void setBloodSugar(String bloodSugar) {

						  this.bloodSugar = bloodSugar;

						  }
						  
						  
						  
						  public String getOverWeight() {

							  return overWeight;

							  }

							  public void setOverWeight(String overWeight) {

							  this.overWeight = overWeight;

							  }
							  
							  
							  
							  
							  public String getSmoking() {

								  return smoking;

								  }

								  public void setSmoking(String smoking) {

								  this.smoking = smoking;

								  }
								  
								  
								  
								  public String getAlcohol() {

									  return alcohol;

									  }

									  public void setAlcohol(String alcohol) {

									  this.alcohol = alcohol;

									  }
									  
									  public String getDailyExercise() {

										  return dailyExercise;

										  }

										  public void setDailyExercise(String dailyExercise) {

										  this.dailyExercise = dailyExercise;

										  }insuranceAmount
										  
										  
										  public String getDrugs() {

											  return drugs;

											  }

											  public void setDrugs(String drugs) {

											  this.drugs = drugs;

											  }
											  
											  public String getInsuranceAmount() {

												  return insuranceAmount;

												  }

												  public void setInsuranceAmount(String insuranceAmount) {

												  this.insuranceAmount = insuranceAmount;

												  }
												  
												  public String getAge() {

													  return age;

													  }

													  public void setAge(String age) {

													  this.age = age;

													  }

}
